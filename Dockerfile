FROM python:3.9
RUN pip install py-cord
RUN pip install requests
COPY . .
CMD ["python", "./main.py"]
