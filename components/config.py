from json import load, dump
from config.properties import env


class Config:
    def __init__(self, bot=None, config: dict = None):
        self.bot = bot
        self.prefix = None
        self.join_role_name = None
        if config is None:
            self.load_config_from_file()
        else:
            self.apply_config(config)

    def reload(self):
        self.load_config_from_file()

    def save(self):
        self.save_config_to_file()

    def load_config_from_file(self):
        with open('./data/config.json') as c:
            s_config = load(c)
        config = {
            'prefix': s_config['prefix'][env],
            'join_role_name': s_config['join_role_name']
        }
        self.apply_config(config)

    def save_config_to_file(self):
        with open('./data/config.json') as c:
            s_config = load(c)
        s_config['prefix'][env] = self.prefix
        s_config['join_role_name'] = self.join_role_name
        with open('./data/config.json', 'w') as c:
            dump(s_config, c)

    def apply_config(self, config: dict):
        self.prefix = config['prefix']
        self.join_role_name = config['join_role_name']

    def change_prefix(self, new_prefix):
        self.prefix = new_prefix
