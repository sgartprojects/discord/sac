success_emoji = '☑'
fail_emoji = '❌'

white = '\033[0m'  # white (normal)
red = '\033[31m'  # red
green = '\033[32m'  # green
orange = '\033[33m'  # orange
blue = '\033[34m'  # blue
purple = '\033[35m'  # purple
