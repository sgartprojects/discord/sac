import discord
from discord.ext import commands
from components.config import Config
from helpers.logger import log


class Events(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_member_join(self, member):
        config = Config()
        join_role = discord.utils.get(member.guild.roles, name=config.join_role_name)

        welcome_channel = discord.utils.get(member.guild.text_channels, name='willkomme')
        news_channel = discord.utils.get(member.guild.text_channels, name='news')
        info_channel = discord.utils.get(member.guild.text_channels, name='informatione')
        main_channel = discord.utils.get(member.guild.text_channels, name='main')
        media_channnel = discord.utils.get(member.guild.text_channels, name='media')
        commands_channel = discord.utils.get(member.guild.text_channels, name='bot-commands')

        sac_commander = discord.utils.get(member.guild.roles, name='SAC-Commander')

        try:
            await member.add_roles(join_role)
        except Exception as e:
            log('Error while adding join role to user', 'error', e, True)
        embed = discord.Embed (
            title='Willkomme uf em SwissArmaCommunity Server!',
            description='I dere Nachricht findsch du nützlichi Chats, Befehl für de SAC Bot und wiiteri Informatione',
            color=member.color
        )
        embed.add_field(
            name='Nützlichi Chats',
            value=f'{news_channel.mention}\tServernews, Eventakündigunge, etc.\n'
                  f'{info_channel.mention}\tAktuelli Information wie Modpacks, Links, Rolle, Videos, etc.\n'
                  f'{main_channel.mention}\tGlobali Serverchat mit allne User\n'
                  f'{media_channnel.mention}\tMedia channel für Bilder, Clips, etc.\n'
                  f'{commands_channel.mention}\tChannel für Befehl an Bot',
            inline=False
        )
        embed.add_field(
            name='Nützlichi Befehl',
            value=f'``{await self.bot.command_prefix("", "")}help``\tLah dir all Befehl aazeige\n'
                  f'``{await self.bot.command_prefix("", "")}info``\tSchickt dir das Embed nomal mit de aktuelle Date\n'
                  f'``{await self.bot.command_prefix("", "")}unit recruit``\tSchriib dich für e Unit ii\n'
                  f'``{await self.bot.command_prefix("", "")}unit transfer``\tTransfer zunere andere Unit\n'
                  f'``{await self.bot.command_prefix("", "")}modules learn``\tSchriib dich für es verfüegbars Modul ii\n'
        )
        embed.add_field(
            name='Bi frage und für wiiteri Informatione',
            value=f'Uf en {sac_commander.mention} zuecho und fräge'
        )
        try:
            await member.send(embed=embed)
            log('Sent info embed to member', 'success')
        except Exception as e:
            log('Unable to send info embed to member! Proceeding to send it to welcome channel...', 'warning', e)
            if welcome_channel is None:
                log('Unable to send info embed to welcome channel', 'error', 'Welcome channel unknown', True)
                return
            await welcome_channel.send(f'Hey {member.mention}, ha di per DM nöd chöne erreiche, '
                                       f'daher schick ich dir dInfos da in channel', embed=embed)


def setup(bot):
    bot.add_cog(Events(bot))
