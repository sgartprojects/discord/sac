from discord.ext import commands
from helpers.logger import log
from asyncio import TimeoutError


class Exceptions(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        command = ctx.command
        cmd_name = "UnknownCommand"
        if command is not None:
            cmd_name = command.name

        if isinstance(error, commands.CommandNotFound):
            await ctx.reply('De Befehl kenn ich nöd! Hesch en richtig gschribe?')
            log(f'Error while executing command "{cmd_name}"', 'error', error, False)
            return
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.reply(f'Mir fehled benötigti Informatione! Bitte nomal probiere\n```{error}```')
            log(f'Error while executing command "{cmd_name}"', 'error', error, False)
            return
        if isinstance(error, commands.BadArgument):
            await ctx.reply(f'Falschi Informatione! Bitte nomal probiere\n```{error}```')
            log(f'Error while executing command "{cmd_name}"', 'error', error, False)
            return
        if isinstance(error, commands.BotMissingPermissions):
            await ctx.reply('Mir fehled benötigti Berechtigunge! Beschwer dich bim Beetle')
            log(f'Error while executing command "{cmd_name}"', 'error', error, True)
            return
        if isinstance(error, commands.MissingAnyRole):
            await ctx.reply('Dir fehled benötigti Berechtigunge!')
            log(f'Error while executing command "{cmd_name}"', 'error', error, False)
            return
        if isinstance(error, commands.CommandOnCooldown):
            await ctx.reply('De Befehl isch grad no am abchüele :)! Bitte später nomal probiere')
            log(f'Error while executing command "{cmd_name}"', 'error', error, False)
            return
        if isinstance(error, commands.DisabledCommand):
            await ctx.reply('De Befehl isch im Moment deaktiviert!')
            log(f'Error while executing command "{cmd_name}"', 'error', error, False)
            return
        if isinstance(error, commands.NoPrivateMessage):
            await ctx.reply('De Befehl cha nur uf em Server usgfüehrt werde!')
            log(f'Error while executing command "{cmd_name}"', 'error', error, False)
            return
        if isinstance(error, commands.PrivateMessageOnly):
            await ctx.reply('De Befehl cha nur i de DMs uusgführt werde!'),
            log(f'Error while executing command "{cmd_name}"', 'error', error, False)
            return
        if isinstance(error, TimeoutError):
            await ctx.reply('Timed out. Bitte nomal neu probiere')
            log(f'Error while executing command "{cmd_name}"', 'error', error, False)
            return
        await ctx.reply('Unbekannte Fehler! Bitte nomal probiere.')
        log(f'Error while executing command "{cmd_name}"', 'error', error, True)


def setup(bot):
    bot.add_cog(Exceptions(bot))
