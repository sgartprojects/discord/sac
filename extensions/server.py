import discord
from discord.ext import commands
from components.config import Config
from helpers.logger import log


class Server(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        name='info',
        aliases=['i']
    )
    @commands.guild_only()
    async def info(self, ctx):
        news_channel = discord.utils.get(ctx.guild.text_channels, name='news')
        info_channel = discord.utils.get(ctx.guild.text_channels, name='informatione')
        main_channel = discord.utils.get(ctx.guild.text_channels, name='main')
        media_channnel = discord.utils.get(ctx.guild.text_channels, name='media')
        commands_channel = discord.utils.get(ctx.guild.text_channels, name='bot-commands')

        sac_commander = discord.utils.get(ctx.guild.roles, name='SAC-Commander')

        embed = discord.Embed (
            title='Informatione zum SwissArmaCommunityServer',
            description='I dere Nachricht findsch du nützlichi Chats, Befehl für de SAC Bot und wiiteri Informatione',
            color=ctx.author.color
        )
        embed.add_field(
            name='Nützlichi Chats',
            value=f'{news_channel.mention}\t{news_channel.description}\n'
                  f'{info_channel.mention}\t{info_channel.mention}\n'
                  f'{main_channel.mention}\tGlobali Serverchat mit allne User\n'
                  f'{media_channnel.mention}\tMedia channel für Bilder, Clips, etc.\n'
                  f'{commands_channel.mention}\tChannel für Befehl an Bot',
            inline=False
        )
        embed.add_field(
            name='Nützlichi Befehl',
            value=f'``{await self.bot.command_prefix("", "")}help``\tLah dir all Befehl aazeige\n'
                  f'``{await self.bot.command_prefix("", "")}info``\tSchickt dir das Embed nomal mit de aktuelle Date\n'
                  f'``{await self.bot.command_prefix("", "")}unit recruit``\tSchriib dich für e Unit ii\n'
                  f'``{await self.bot.command_prefix("", "")}unit transfer``\tTransfer zunere andere Unit\n'
                  f'``{await self.bot.command_prefix("", "")}module learn``\tSchriib dich für es verfüegbars Modul ii\n',
            inline=False
        )
        embed.add_field(
            name='Bi frage und für wiiteri Informatione',
            value=f'Uf en {sac_commander.mention} zuecho und fräge',
            inline=False
        )
        await ctx.reply(embed=embed)


def setup(bot):
    bot.add_cog(Server(bot))
