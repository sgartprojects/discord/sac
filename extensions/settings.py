import discord
from discord.ext import commands
from helpers.logger import log
from components.config import Config


class Settings(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.config = Config()

    async def before_invoke(self, ctx):
        self.config.reload()

    @commands.group(
        name='settings',
        aliases=['s']
    )
    @commands.has_any_role('Admin')
    @commands.guild_only()
    async def settings(self, ctx):
        if ctx.invoked_subcommand is None:
            raise commands.CommandNotFound

    @settings.group(
        name='set',
        aliases=['s']
    )
    @commands.before_invoke(before_invoke)
    async def set(self, ctx):
        if ctx.invoked_subcommand is None:
            raise commands.CommandNotFound

    @set.command(
        name='prefix',
        aliases=['p']
    )
    async def prefix(self, ctx, prefix: str):
        self.config.prefix = prefix
        new_prefix = self.config.prefix
        self.bot.command_prefix = new_prefix
        self.config.save()
        await ctx.reply(f'Prefix isch zu "{new_prefix}" gänderet worde!')
        log(f'Prefix was changed to "{new_prefix}"', 'success', None, True)

    @set.group(
        name='channel',
        aliases=['c']
    )
    async def channel(self, ctx):
        if ctx.invoked_subcommand is None:
            raise commands.CommandNotFound

    @channel.command(
        name='welcome',
        aliases=['w']
    )
    async def welcome(self, ctx, welcome_channel: discord.TextChannel):
        self.config.welcome_channel_id = welcome_channel.id
        self.config.save()
        await ctx.reply(f'Channel isch zu {welcome_channel.mention} gänderet worde!')
        log(f'Welcome channel was changed to "{welcome_channel.name}" ({welcome_channel.id})', 'success', None, True)

    @set.group(
        name='role',
        aliases=['r']
    )
    async def role(self, ctx):
        if ctx.invoked_subcommand is None:
            raise commands.CommandNotFound

    @role.command(
        name='join',
        aliases=['j']
    )
    async def join(self, ctx, role: discord.Role):
        self.config.join_role_id = role.id
        self.config.save()
        await ctx.reply(f'Join-Rolle isch zu "{role.name}" gänderet worde!')
        log(f'Join role was changed to "{role.name}" ({role.id})', 'success', None, True)


def setup(bot):
    bot.add_cog(Settings(bot))
