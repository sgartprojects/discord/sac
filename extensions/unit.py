import discord
from discord.ext import commands
from constants import success_emoji, fail_emoji


class Server(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group(
        name='unit',
        aliases=['u']
    )
    async def unit(self, ctx):
        if ctx.invoked_subcommand is None:
            raise commands.CommandNotFound

    @unit.command(
        name='recruit',
        aliases=['r']
    )
    @commands.dm_only()
    async def recruit(self, ctx):
        def is_not_bot(reaction, user):
            return not user.bot

        recruit_msg = await ctx.reply('Willkomme bi de Unit Iischriibig.\n'
                                      'Zum abbreche chasch jederziit ``cancel`` schriibe.\n'
                                      'Du hesch jewiils 5 Minute ziit zum e Frag beantworte. '
                                      'Nach ablauf vo de Ziit muess dIischribig neu gstartet werde')
        await recruit_msg.reply('Für welli Sektion wetsch du dich iischribe?\n'
                                'Möglichi Sektione sind 20th Armoured Infantry Brigade (20AIB) '
                                'und Joint Helicopter Command (JHC).\n'
                                '**Prüef vor em Iischriibe im Info-Channel ob die gwünschti Sektion verfüegbar isch!**')
        unit_msg = await self.bot.wait_for('message', timeout=300)
        unit = unit_msg.content

        await recruit_msg.reply('Wie wetsch du gennt werde?\n'
                                'Bitte reali Näme im Format wie "R. Smith" oder "S. Davies" näh')
        name_msg = await self.bot.wait_for('message', timeout=300)
        name = name_msg.content

        await recruit_msg.reply('Was isch din Steam Fründescode?\n'
                                'Fründescode wird nur vo de Commander ersichtlich sii und dient lediglich zum '
                                'dFründschaftsafrag vereifache!\n'
                                'Weisch nöd wo du de Code findsch? De folgendi Artikel cha helfe: '
                                'https://www.howtogeek.com/671958/how-to-find-a-friend-code-in-steam/\n'
                                '*Für die Frag hesch du 10 Minute Ziit zum antworte*')
        code_msg = await self.bot.wait_for('message', timeout=600)
        code = code_msg.content

        await recruit_msg.reply('Hesch du bereits ArmA Milsim erfahrig? (Ja, Etwas, Nein)')
        exp_msg = await self.bot.wait_for('message', timeout=300)
        exp = exp_msg.content

        end_question_msg = await recruit_msg.reply('Iischribig abschicke?\n'
                          '**Mit em Abschicke erklärsch du dich bereit, '
                          'a mindestens 3 (Trainings-)Iisätz im Monet debii zsi**')
        await end_question_msg.add_reaction(success_emoji)
        await end_question_msg.add_reaction(fail_emoji)
        end_choice_reaction = await self.bot.wait_for('reaction_add', check=is_not_bot, timeout=300)
        end_choice = end_choice_reaction[0].emoji
        if end_choice == fail_emoji:
            await recruit_msg.reply('Iischribig abbroche!')
            return

        recruit_channel = discord.utils.get(
            discord.utils.get(self.bot.guilds, id=886566367539646495).text_channels, name='iischribige')
        embed = discord.Embed(
            title='Neui iischribig',
            color=discord.Color.red()
        )
        embed.add_field(
            name='User',
            value=ctx.author.mention,
            inline=False
        )
        embed.add_field(
            name='Unit',
            value=unit,
            inline=False
        )
        embed.add_field(
            name='Name',
            value=name,
            inline=False
        )
        embed.add_field(
            name='Steam Fründescode',
            value=f'||{code}||',
            inline=False
        )
        embed.add_field(
            name='Erfahrig (Ja, Etwas, Nein)',
            value=exp,
            inline=False
        )
        await recruit_channel.send(embed=embed)
        await recruit_msg.reply('Iischribig abgschickt!')

    @unit.command(
        name='transfer',
        aliases=['t']
    )
    @commands.dm_only()
    async def transfer(self, ctx):
        await ctx.reply('Zurziit sind kei Transfers möglich!')


def setup(bot):
    bot.add_cog(Server(bot))
