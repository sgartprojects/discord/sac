from datetime import datetime

from helpers import webhook
from constants import red, green, blue, white, orange


datestamp = datetime.now().strftime('%Y-%m-%d')
timestamp = datetime.now().strftime('%H:%M:%S')


def log(message, log_type=None, error=None, post_in_discord=False):
    out = ''
    color = white
    if log_type == 'error':
        out += ' [ERR]'
        color = red
    if log_type == 'success':
        out += ' [SUC]'
        color = green
    if log_type == 'warning':
        out += ' [WRN]'
        color = orange
    if log_type == 'info':
        out += ' [INF]'
        color = blue
    if log_type is None:
        out += ' [NAN]'
    out += f" {message}"
    d_out = out
    if log_type == 'error':
        out += f': {error}'
        d_out += f': ``{error}``'

    print(f'{color}[{datestamp}] [{timestamp}]{out}{white}')
    if post_in_discord:
        try:
            response = webhook.send_message(d_out)
            if response.status_code != 204:
                log(f"Error while sending webhook request (Code: {response.status_code})", 'error', response.text)
            else:
                log(f"Webhook sent successfully (Code: {response.status_code})", 'success')
        except Exception as e:
            log('Error while sending message to discord', 'error', e)
