import requests

from secrets import webhooks


def send_message(message):
    data = {
        'content': message
    }
    response = requests.post(webhooks['log'], data)
    return response


def send_embed(embed, webhook):
    data = {
        'embeds': [embed]
    }
    response = requests.post(webhook, data)
    return response
