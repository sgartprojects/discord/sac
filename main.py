import discord

from components.config import Config
from discord.ext import commands
from os import listdir
from helpers.logger import log
from secrets import token

log('Starting up...', 'info')
log('Loading config...', 'info')
config = Config()
log('Config loaded', 'success')
log('Creating bot instance...', 'info')


async def get_prefix(client, message):
    return config.prefix


intents = discord.Intents.all()
bot = commands.Bot(command_prefix=get_prefix, case_insensitive=True, intents=intents)
config.bot = bot
log('Bot instance created', 'success')

log('Loading extensions...', 'info')
all_ex = 0
suc_loaded = 0
for filename in listdir('./extensions'):
    if filename.endswith('.py'):
        all_ex += 1
        log(f'Loading extension "{filename[:-3]}"...', 'info')
        try:
            bot.load_extension(f'extensions.{filename[:-3]}')
            suc_loaded += 1
            log(f'Loading of extension "{filename[:-3]}" done', 'success')
        except Exception as e:
            log(f'Error while loading extension "{filename[:-3]}"', 'error', e, True)
if all_ex != suc_loaded:
    log(f'Not all extensions have been loaded ({suc_loaded}/{all_ex} successful)', 'warning')
else:
    log('All extensions loaded', 'success')

@bot.event
async def on_ready():
    log(f'Startup successful! Prefix: "{config.prefix}"', 'success', None, False)


bot.run(token)
